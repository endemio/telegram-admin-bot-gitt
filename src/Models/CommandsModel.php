<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 17.05.2018
 * Time: 20:14
 */

namespace SpamDeletingBot\Models;


use SpamDeletingBot\Controllers\Bot;
use SpamDeletingBot\Controllers\Data;
use SpamDeletingBot\Controllers\Logger;

class CommandsModel
{

    private $logger;

    private $data;

    private $request;

    private $bot;

    private $bot_config;

    public function __construct($logger,$request) {
        $this->logger = (empty($logger))?new Logger():$logger;

        $this->data                 = new Data($logger);

        $this->bot                  = new Bot($logger);

        $this->request              = $request;
    }

    public function callCommandFunctionModel($function,$data) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        if (method_exists($this, $function)) {
            $result = call_user_func(array($this, $function), $data);
        } else {
            $this->logger->error('Function '.$function.' in class '.__CLASS__.' not found');
            return false;
        }

        return $result;
    }

    public function userStatusModel($user){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->bot->getUserStatus($user);
    }

    public function getBotConfigModel(){
        return $this->bot->getBotConfig();
    }

    public function banUserModel($id){
        return $this->addToList('banned',array('text'=>'','vars'=>$id));
    }

    private function startMessage ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        $this->logger->info(print_r($input,true));

        return [];
    }

    private function showHelp ($input) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $commands_list = $this->data->getYamlFile(CONFIG_DIR.'routes.yaml')['routes'];


        $result =[];
        foreach ($commands_list as $key=>$value){
            if ($value['private']) {
                array_push($result, array('command' => str_replace('-private','',$key), 'text' => $value['name']));
            }
        }

        return array('list'=>$result);
    }

    private function showCurrentConfiguration($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $settings = $this->getBotConfigModel();

        $route = $this->data->getYamlFile(CONFIG_DIR.'routes.yaml')['routes'];

        $result =[];

        foreach ($route as $key=>$value){
            if (!empty($value['configkey'])){
                print $value['configkey'];
                array_push($result, array('text' => $value['name'],'value'=>$settings[$value['configkey']]));
            }
        }

        return array('list'=>$result);
    }

    private function showWelcomeMessage ($input) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return array('welcome'=>$this->bot->getBotConfig()['welcome']);
    }

    private function setWelcomeMessage ($input) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        // Get bot config
        $this->bot_config = $this->bot->getBotConfig();

        if (empty($input['command']['vars'])) {
            return array(
                'result' => 'error',
                'text'=>"Please use the correct command structure: ".$input['command']['text']." <i>text text text</i>\r\n<b>Note:</b> Add {username} to text. It will be changed to new member first name and second name");
        }

        $this->bot_config['welcome'] = $input['command']['vars'];

        // Check if data save
        $result = $this->bot->saveConfig($this->bot_config);
        #$result = true;

        if ($result) {
            return array('result' => 'success','message' => 'Welcome message was successful added','text'=>$input['command']['vars']);
        } else {
            return array('result' => 'error', 'message' => 'Data was NOT stored');
        }
    }

    private function showWhitelist($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return array('list'=>$this->showList('whitelist'));
    }

    private function addUserToWhitelist($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->addToList('whitelist',$input['command']);
    }

    private function removeUserFromWhitelist($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->removeFromList('whitelist',$input['command']);
    }

    private function showAdmins($input){
        return array('list'=>$this->showList('admins'));
    }

    private function addUserToAdmins($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->addToList('admins',$input['command']);
    }

    private function removeUserFromAdmins($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->removeFromList('admins',$input['command']);
    }

    private function showBanned($input){
        return array('list'=>$this->showList('banned'));
    }

    private function addUserToBanned($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->addToList('banned',$input['command']);
    }

    private function removeUserFromBanned($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->removeFromList('banned',$input['command']);
    }

    private function changeKickoff ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->changeBool('kickoff user','kickoff');
    }

    private function changeDeleteMessages ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->changeBool('delete message','deletemessage');
    }

    private function changeRemovePhoto ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->changeBool('remove photo','removephoto');
    }

    private function changeRemoveMessagesWithURL ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->changeBool('remove messages with urls','removeurls');
    }

    private function changeRemoveMessagesWithBadWords ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->changeBool('remove messages with bad words','removebadwords');
    }

    /**
     * Return lists from config
     * @param $list
     * @return array
     */
    private function showList ($list) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        if (isset($this->getBotConfigModel()[$list])) return $this->getBotConfigModel()[$list];

        return [];
    }

    /**
     * Add element to list
     * @param $list
     * @param $command
     * @return array
     */
    private function addToList ($list,$command) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        $this->logger->info('List =>'.$list);
        $this->logger->info(print_r($command,true));

        // Get bot config
        $this->bot_config = $this->bot->getBotConfig();


        // Check if there is white list in config files
        if (empty($this->bot_config[$list])){
            $this->bot_config[$list] = [];
        }

        if (empty($command['vars'])) {
            return array(
                'result' => 'error',
                'message'=>"Please use the correct command structure: ".$command['text']." <i>Username</i>\r\n<b>Note:</b> Do not include @");
        }

        // explode url if not only the on in command
        $elements = preg_split('/[\s,]+/',$command['vars']);

        // add urls to config
        foreach ($elements as $element){
            array_push($this->bot_config[$list],$element);
        }

        // remove duplicates
        $this->bot_config[$list]=array_unique($this->bot_config[$list],SORT_STRING );

        $this->logger->info(print_r($this->bot_config[$list],true));

        // Check if data save
        $result = $this->bot->saveConfig($this->bot_config);

        if ($result) {
            return array('result' => 'success', 'message' => '🧙 <b>'.implode(', ',$elements).'</b> was successful added to list');
        } else {
            return array('result' => 'error', 'message' => 'Data was NOT stored');
        }

    }

    /**
     * Remove elements from config save it
     * @param $list
     * @param $command
     * @return array
     */
    private function removeFromList ($list,$command) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        $this->logger->info('List =>'.$list);
        $this->logger->info(print_r($command,true));

        // Get bot config
        $this->bot_config = $this->bot->getBotConfig();


        // Check if there is white list in config files
        if (empty($this->bot_config[$list])){
            $this->bot_config[$list] = [];
        }

        if (empty($command['vars'])) {
            return array(
                'result' => 'error',
                'message'=>"Please use the correct command structure: ".$command['text']." <i>Username</i>\r\n<b>Note:</b> Do not include @");
        }

        // explode url if not only the on in command
        $elements = preg_split('/[\s,]+/',$command['vars']);

        foreach ($elements as $element){
            $error = '';
            if (!in_array($element,$this->bot_config[$list])) {
                $error.='   - '.$element." not found in current bots white list\r\n";
            }
        }

        $this->logger->info(print_r($this->bot_config[$list],true));

        if (empty($error)) {
            $this->bot_config[$list] = array_diff($this->bot_config[$list], $elements);
        } else {
            return array('result' => 'error', 'message' => $error);
        }

        // Check if data save
        $result = $this->bot->saveConfig($this->bot_config);
        #$result = true;

        if ($result) {
            return array('result' => 'success','message' => '<b>'.implode(', ',$elements).'</b> was successful removed from list');
        } else {
            return array('result' => 'error', 'message' => 'Data was NOT stored');
        }
    }

    private function changeBool ($text,$key) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $result = $this->bot->changeBoolValue($key);

        if ($result) {
            return array('result' => 'success', 'message' => $text.' - <b>'.$result['value'].'</b>');
        } else {
            return array('result' => 'error', 'message' => 'Data was NOT stored');
        }
    }

}