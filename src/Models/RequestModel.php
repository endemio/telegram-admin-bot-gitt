<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 22.03.2018
 * Time: 4:25
 */

namespace SpamDeletingBot\Models;
use SpamDeletingBot\Controllers\Data;
use SpamDeletingBot\Controllers\Logger;

class RequestModel
{

    protected $request;

    private $data;

    private $logger;

    public function __construct($logger) {

        $this->logger = (empty($logger))?new Logger():$logger;

        $this->data = new Data($logger);
    }

    public function storeRequestModel () {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);

        # get request from php://input
        $input = file_get_contents('php://input');

        # If empty request - get fake request data
        if (empty($input)) {
            $input = $this->testRequestData();
        }

        $this->request = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $input), false, JSON_UNESCAPED_UNICODE);

        $this->saveRequest($this->request);

        return true;
    }

    public function getRequestModel () {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->request;
    }


    public function saveRequest($input){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $chat_id = $this->getChatIdModel();

        $user_id = $this->getUserDataModel()->id;

        $message_id = $this->getMessageIdModel();

        if (!empty($user_id) && !empty($chat_id)) {
            if ($chat_id == $user_id) {
                file_put_contents(LOG_DIR . 'direct-'.date("Y-m-d") . '.txt', json_encode($input). PHP_EOL, FILE_APPEND);
            } else {
                file_put_contents(LOG_DIR . 'chat-'.$chat_id.'-'.date("Y-m-d") . '.txt', json_encode($input). PHP_EOL, FILE_APPEND);
            }

            if (!is_dir(DATA_DIR.'users')) mkdir(DATA_DIR . 'users',0755);
            file_put_contents(DATA_DIR . 'users/'.$user_id.'.log', $message_id. PHP_EOL, FILE_APPEND);
        }
    }

    public function getCommandModel(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);

        if (empty($this->request->message->text)) return false;

        $message = $this->request->message->text;

        preg_match('/\/(\w+)\s(.*)/',$message, $matches);
        if (!empty($matches)){
            return array('text'=>'/'.$matches[1],'vars'=>$matches[2]);
        }

        preg_match('/\/(\w+)(.*)/',$message, $matches);
        if (!empty($matches)){
            return array('text'=>'/'.$matches[1],'vars'=>null);
        }

        return [];
    }

    public function getCallbackModel(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        if (!empty($this->request->callback_query->data)) return $this->request->callback_query->data;

        return false;

    }

    public function getChatIdModel(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        if (empty($this->request->message->chat->id)){
            $this->logger->error('Chat_id not found. Bot stopped');
            exit;
        }
        return $this->request->message->chat->id;
    }

    public function getChatNameModel(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->request->message->chat->title;
    }

    public function getChatUserNameModel(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->request->message->chat->username;
    }

    /**
     * Get user's data
     * @return mixed
     */
    public function getUserDataModel() {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        if (!empty($this->request->message->from)) return $this->request->message->from;
        if (!empty($this->request->callback_query->from)) return $this->request->callback_query->from;
        return [];
    }

    public function getNewUsersModel() {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        if (!empty($this->request->message->new_chat_members)) {
            return $this->request->message->new_chat_members;
        } else {
            return [];
        }
    }

    public function getMessageTextModel(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        if (!empty($this->request->message->text))   return $this->request->message->text;
        if (!empty($this->request->callback_query->message->text))   return $this->request->callback_query->message->text;
        return null;
    }

    public function getMessageModel(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        $result                 = $this->getMessageTypeModel($this->request);
        $result['text']         = $this->getMessageTextModel();
        $result['chat_id']      = $this->getChatIdModel();
        $result['message_id']   = $this->getMessageIdModel();
        $result['user']         = $this->getUserDataModel();
        $result['is_command']   = $this->isCommand();
        if ($this->isCommand()){
            $result['command']  = $this->getCommandModel();
        }
        $result['is_callback']  = $this->isCallback();
        $result['is_direct']    = $this->isDirectMessage();
        $result['urls_message'] = $this->getUrlsFromMessageModel();
        $result['urls_username']= $this->getUrlsFromUsernameModel();
        $result['bad_words']    = $this->checkBadWordsModel($this->allMessageText());

        if ($result['type'] == 'reply_to_message'){
            $result['replied_message'] = (array)$this->request->message->reply_to_message;
        }

        return $result;
    }

    public function getMessageIdModel() {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        if (!empty($this->request->message->message_id)) return $this->request->message->message_id;
        if (!empty($this->request->callback_query->message->message_id)) return $this->request->callback_query->message->message_id;
        return false;
    }


    /**
     * Get Mesages type (photo, message, text, sticker....)
     * @param $input
     * @return array
     */
    public function getMessageTypeModel($input) {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        $result['type'] = 'message';

        #check callback
        if (!empty($input->callback_query)) {
            $result['type'] = 'callback';
            $result['callback_string'] = $input->callback_query->data;
            $result['callback_query_id'] = $input->callback_query->id;
        }

        #check new member
        if (!empty($input->message->new_chat_member)) {
            $result['type'] = 'new_chat_member';
        }

        #check messages by member
        if (!empty($input->message->sticker)){
            $result['type'] = 'sticker';
        }

        #check messages by member
        if (!empty($input->message->reply_to_message)){
            $result['type'] = 'reply_to_message';
        }

        #check messages by member
        if (!empty($input->message->pinned_message)){
            $result['type'] = 'pinned_message';
        }

        #check messages by member
        if (!empty($input->message->document)){
            $result['type'] = 'document';
        }

        #forwarded
        if (!empty($input->message->forward_from_chat)){
            $result['type']  = 'forward_from_chat';
        }

        #forwarded
        if (!empty($input->message->photo)){
            $result['type']  = 'photo';
        }

        #forwarded
        if (!empty($input->message->contact)){
            $result['type']  = 'contact';
        }

        $result['id'] = $this->getMessageIdModel();
        return $result;
    }

    /**
     * Function get test data from test-folder
     */
    private function testRequestData (){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $requests = $this->data->getYamlFile(DATA_DIR.'/test-request/request.yaml')['requests'];

        $requests = (empty($requests))?array(0=>''):$requests;

        $num = (isset($_GET['request']))?(int)htmlspecialchars($_GET['request']):'';

        if (isset($_GET['fake'])) return false;

        if (!empty($num) && $num < count($requests)-1) {
            return $requests[$num];
        } else {
            return $requests[rand(0, count($requests) - 1)];
        }
    }

    private function isCommand () {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        if (!empty($this->getRequestModel()->message->entities[0]->type) && $this->getRequestModel()->message->entities[0]->type == 'bot_command') return true;
        return false;
    }

    private function isDirectMessage(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return ($this->getUserDataModel()->id == $this->getChatIdModel())?true:false;
    }

    private function isCallback(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return (!empty($this->getRequestModel()->callback_query))?true:false;
    }

    public function checkBadWordsModel($text){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);

        // get bad words from yaml
        $bad_words = $this->data->getYamlFile(CONFIG_DIR.'badwords.yaml');

        // for every bad word try find matches in string
        foreach ($bad_words as $value){
            // find matches and positions
            preg_match_all('/\b'.strtolower($value).'\b/i',strtolower($text),$matches,PREG_OFFSET_CAPTURE);

            print_r($matches);

            if (!empty($matches[0][0])){
                return true;
            }
        }
        return false;
    }

    public function getUrlsFromMessageModel() {

        $text = $this->allMessageText();

        $result = $this->getUrlsFromTextRouteModel($text);

        return $result;
    }

    public function getUrlsFromUsernameModel() {

        $result = [];

        if (!empty($this->request->message->from->first_name)){
            $result['first_name'] = ($this->getUrlsFromTextRouteModel(strtolower($this->request->message->from->first_name))) ? true : false;
        }

        if (!empty($this->request->message->from->last_name)){
            $result['last_name'] = ($this->getUrlsFromTextRouteModel(strtolower($this->request->message->from->last_name))) ? true : false;
        }

        if (!empty($this->request->message->from->username)){
            $result['username'] = ($this->getUrlsFromTextRouteModel(strtolower($this->request->message->from->username))) ? true : false;
        }

        $counter = 0;
        foreach ($result as $value){
            if ($value) $counter++;
        }

        return $counter;
    }

    private function getUrlsFromTextRouteModel($input) {
        #$input = strtolower($input);

        $regex = "((https?|ftp)\:\/\/)?"; // Checking scheme
        $regex .= "([a-zA-Z0-9-.]*)\.([a-z]{2,5})"; // Checking host name and/or IP
        $regex .= "(\:[0-9]{2,5})?"; // Check it it has port number
        $regex .= "(\/([a-zA-Z0-9+\$_-]\.?)+)*\/?"; // The real path
        $regex .= "(\?[a-zA-Z+&\$_.-][a-zA-Z0-9;:@&%=+\/\$_.-]*)?"; // Check the query string params
        $regex .= "(#[a-zA-Z_.-][a-zA-Z0-9+\$_.-]*)?"; // Check anchors if are used.

        preg_match_all('/'.$regex.'/', $input, $matches);

        return $matches[0];
    }

    private function allMessageText(){

        $text = '';

        if (!empty($this->request->message->text)) $text.=$this->request->message->text;
        if (!empty($this->request->message->caption)) $text.=$this->request->message->caption;
        if (!empty($this->request->message->entities)) {
            foreach ($this->request->message->entities as $key=>$value){
                if (!empty($value->url))
                    $text.=' '.$value->url.' ';
            }

            if (!empty($this->request->message->caption))  $text.=$this->request->message->caption;
        }

        return $text;
    }

}