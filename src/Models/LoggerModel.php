<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 15.07.2018
 * Time: 2:25
 */

namespace SpamDeletingBot\Models;
use Katzgrau\KLogger\Logger as KatLogger;
use Psr\Log\LogLevel;

class LoggerModel
{
    private $log;

    private $array_messages = [];

    public function __construct() {
        # Logger
        $this->log = new KatLogger(
            LOG_DIR.'info/'.date("Y-m-d"),
            LogLevel::DEBUG,array('filename'=>'log_from_'.date("H").'_00.txt'));
        $this->log->info(__CLASS__.' '.__FUNCTION__);
    }

    public function infoModel($message){

        if (empty($this->array_messages[$message])) $this->array_messages[$message] = 0;

        $this->array_messages[$message]++;

        if (DEBUG) $this->log->info('['.$this->array_messages[$message].'] '.$message);

        if (DEBUG) print $message.PHP_EOL;
    }

    public function errorModel($message){
        $this->log->error(print_r($message,true));
        $log= $this->miscData();
        $log['message'] = 'Error';
        $log['type'] = 'error';
        $log['error'] = print_r($message,true);
        file_put_contents(LOG_DIR.'error'.'.log',json_encode($log).PHP_EOL,FILE_APPEND);
        if (DEBUG) print $message.PHP_EOL;

    }

    public function warnModel($message){
        $this->log->warning(print_r($message,true));
        $log= $this->miscData();
        $log['message'] = 'Warning';
        $log['type'] = 'warning';
        $log['warning'] = print_r($message,true);
        file_put_contents(LOG_DIR.'warn'.'.log',json_encode($log).PHP_EOL,FILE_APPEND);
        if (DEBUG) print $message.PHP_EOL;
    }

    public function storeModel($input){
        $input+=$this->miscData();
        file_put_contents(LOG_DIR.'info'.'.log',json_encode($input).PHP_EOL,FILE_APPEND);
    }


    private function miscData(){
        $log['@timestamp'] = date('c',time());
        $log['application'] = 'telegram-admin-chat';
        $log['memory_usage'] = memory_get_usage();
        return $log;
    }
}