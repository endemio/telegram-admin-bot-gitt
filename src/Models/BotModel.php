<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 23.04.2018
 * Time: 23:55
 */

namespace SpamDeletingBot\Models;
use SpamDeletingBot\Controllers\Data;
use SpamDeletingBot\Controllers\Logger;
use Spyc;

class BotModel
{
    protected $bot_config;

    private $logger;

    private $config;

    private $data;

    public function __construct($logger) {

        $this->logger = (empty($logger))?new Logger():$logger;

        $this->data = new Data($logger);

        $this->bot_config = $this->getBotConfigModel();

        $this->logger->info(__CLASS__.' '.__FUNCTION__);
    }

    public function getUserStatusModel($user){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $result = array(
            'is_whitelist'=>$this->checkUserInWhitelist($user),
            'is_admin'=>$this->checkUserInAdminList($user),
            'is_banned'=>$this->checkUserInBannedList($user)
        );

        return $result;
    }


    public function checkUserInWhitelist($user){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        if (!empty($user->username)) {
            if (in_array($user->username, $this->getUsersWhitelistModel())) {
                return true;
            }
        }
        return false;
    }


    public function checkUserInAdminList($user){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        if (!empty($user->username)) {
            if (in_array($user->username, $this->getAdminsListModel())) {
                return true;
            }
        }
        return false;
    }

    public function checkUserInBannedList($user){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        print $user->id;

        if (!empty($user->id)) {
            if (in_array($user->id, $this->getBannedListModel())) {
                return true;
            }
        }
        return false;
    }



    public function getUsersWhitelistModel() {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->bot_config['whitelist'];
    }

    private function getAdminsListModel() {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->bot_config['admins'];
    }

    private function getBannedListModel() {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->bot_config['banned'];
    }

    /**
     * Get bot config
     * @return array
     */
    public function getBotConfigModel () {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        return (empty($this->bot_config))?$this->data->getYamlFile('../config/config-main.yaml'):$this->bot_config;

    }

    /**
     * Save this->config to yaml
     * @param $input
     * @return array
     */
    public function saveConfigModel($input) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        if (!is_array($input)) return array('result'=>'error','text'=>'Try save in config not array');

        $yaml = Spyc::YAMLDump($input);

        if (!is_dir(CONFIG_ARCHIVE_DIR)) mkdir(CONFIG_ARCHIVE_DIR,0755);

        if (!empty($yaml)) {
            $result = $this->saveFileWithArchive($yaml, CONFIG_DIR . 'config-main.yaml', CONFIG_ARCHIVE_DIR . 'config-main-' . time() . '.yaml');
        } else {
            $result = false;
        }

        if ($result) return array('result'=>'success','data'=>'Routes Yaml config file saved successful');

        else         return array('result'=>'success','data'=>'Routes Yaml config file NOT saved');
    }

    /**
     * Change bool value (on/off)
     * @param $key
     * @return array
     */
    public function changeBoolValueModel($key){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $this->config = $this->getBotConfigModel();

        // Set new value
        $this->config[$key]=(strtolower($this->config[$key])=='on')?'off':'on';

        // Check if data save
        $result = $this->saveConfigModel($this->config);

        if ($result) $result['value'] = $this->config[$key];

        return $result;
    }

    private function saveFileWithArchive($content, $file_to, $file_archive){

        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $result = true;
        // make copy
        try {
            copy($file_to, $file_archive);
        } catch (\Exception $e) {
            print 'File '.$file_to.' cannot be saved to '.$file_archive;
            print $e->getMessage();
            $result = false;
        }

        // save new content to file
        try {
            file_put_contents($file_to, $content);
        } catch (\Exception $e) {
            print 'Content cannot be saved in '.$file_to;
            print $e->getMessage();
            $result = false;
        }

        return $result;
    }



}