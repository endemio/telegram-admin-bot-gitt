<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 20.02.2018
 * Time: 15:19
 */

namespace SpamDeletingBot\Models;
use SpamDeletingBot\Controllers\Logger;

class ViewModel
{
    private $logger;

    public function __construct() {
        $this->logger = (empty($logger))?new Logger():$logger;

    }

    public function viewRouteModel($function,$data) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        if (method_exists($this, $function)) {
            $result = call_user_func(array($this, $function), $data);
        } else {
            $this->logger->error('Function '.$function.' in class '.__CLASS__.' not found');
            return false;
        }

        return $result;
    }

    private function startMessage($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'];

        return $str;
    }


    private function showHelp ($input) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;
        foreach ($input['data']['list'] as $key=>$value){
            $str.=$value['command'].' - '.$value['text'].TELEGRAM_NEW_LINE;
        }

        return $str;
    }

    private function showWelcomeMessage ($input) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.= $input['data']['welcome'];

        return $str;
    }

    private function setWelcomeMessage ($input) {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.= $input['data']['text'];

        return $str;
    }

    private function showCurrentConfiguration($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        foreach ($input['data']['list'] as $key=>$value){
            $str.=$value['text'].' - <b>'.$value['value'].'</b>'.TELEGRAM_NEW_LINE;
        }

        return $str;

    }

    private function showWhitelist($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        foreach ($input['data']['list'] as $key=>$value){
            $str.=' <b>'.$value.'</b>'.TELEGRAM_NEW_LINE;
        }

        return $str;
    }

    private function addUserToWhitelist($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.=$input['data']['message'];

        return $str;
    }

    private function removeUserFromWhitelist($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.=$input['data']['message'];

        return $str;
    }

    private function showAdmins($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        foreach ($input['data']['list'] as $key=>$value){
            $str.=' <b>'.$value.'</b>'.TELEGRAM_NEW_LINE;
        }

        return $str;

    }

    private function addUserToAdmins($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.=$input['data']['message'];

        return $str;
    }

    private function removeUserFromAdmins($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.=$input['data']['message'];

        return $str;}

    private function showBanned($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        foreach ($input['data']['list'] as $key=>$value){
            $str.=' <b>'.$value.'</b>'.TELEGRAM_NEW_LINE;
        }

        return $str;
    }

    private function addUserToBanned($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.=$input['data']['message'];

        return $str;
    }

    private function removeUserFromBanned($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.=$input['data']['message'];

        return $str;
    }

    private function changeKickoff ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.='Setting '.$input['data']['message'];

        return $str;

    }

    private function changeDeleteMessages ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.='Setting '.$input['data']['message'];

        return $str;

    }

    private function changeRemovePhoto ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.='Setting '.$input['data']['message'];

        return $str;

    }

    private function changeRemoveMessagesWithURL ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.='Setting '.$input['data']['message'];

        return $str;

    }

    private function changeRemoveMessagesWithBadWords ($input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $str = $input['route']['message'].TELEGRAM_NEW_LINE;

        $str.='Setting '.$input['data']['message'];

        return $str;

    }

    private function debugViewFunction ($input) {

        $result['view'] = true;
        $result['text'] = $input['data']['message'];

        return $result;
    }
}