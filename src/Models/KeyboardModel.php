<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 20.02.2018
 * Time: 1:02
 */

namespace SpamDeletingBot\Models;


use SpamDeletingBot\Controllers\Logger;

class KeyboardModel
{
    protected $bot_config;

    private $logger;

    public function __construct($logger) {
        $this->logger = (empty($logger))?new Logger():$logger;
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
    }


    public function setDefaultModel () {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return json_encode(
            array(
                "keyboard" => array(
                        array(
                            array('text' =>"/help"),
                            array('text' =>"/showwl"),
                            array('text' =>"/showadmin"),
                            array('text' =>"/settings"),
                        ),
                ),
                "resize_keyboard" => true,
            )
        );
    }



}