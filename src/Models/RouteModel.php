<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 20.02.2018
 * Time: 1:03
 */

namespace SpamDeletingBot\Models;
use SpamDeletingBot\Controllers\Commands;
use SpamDeletingBot\Controllers\Data;
use SpamDeletingBot\Controllers\Logger;
use SpamDeletingBot\Controllers\Request;

class RouteModel
{
    private $routes;
    private $request;
    private $json;
    private $logger;
    private $command;
    private $data;


    public function __construct($logger) {
        $this->logger = (empty($logger))?new Logger():$logger;

        $this->request  = new Request($logger);

        $this->data = new Data($logger);

        $this->command = new Commands($this->logger,$this->request);

        $this->routes   = $this->data->getYamlFile(CONFIG_DIR.ROUTES_CONFIG)['routes'];
    }


    public function startModel (){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $this->json                 = $this->request->storeRequest();
    }

    public function getBotConfigModel(){
        return $this->command->getBotConfig();
    }

    public function banUserModel($id){
        return $this->command->banUser($id);
    }

    public function listMessagesByUserIdModel($id){
        return $this->data->getYamlFile(DATA_DIR.'/users/'.$id.'.log');
    }

    public function storeListMessagesByUserIdModel($id,$list){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        file_put_contents(DATA_DIR.'/users/'.$id.'.log',implode(PHP_EOL,$list));
    }


    /**
     * Main Route Function
     *
     */
    public function checkRouteModel(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $result = array();

        $message = $this->request->getMessage();

        $this->logger->info(print_r($message,true));

        // Check if user which send command is Admn and send command in private chat
        if ($message['is_command'] && $message['is_direct']) {

            $this->logger->info('!----Private Command ----');

            $result = $this->routeTextCommandModel($message);
        }

        // Check if user send command in chat
        if ($message['is_command'] && !$message['is_direct']) {

            $this->logger->info('!----External Command ----');

            $result = $this->routeTextCommandModel($message);
        }

        // Check if user send callback is whitelist and send command in private chat
        if ($message['is_callback'] && $message['is_direct']) {

            $this->logger->info('!----Callback Command ----');

            $result = $this->routeCallbackModel($message);
        }

        $result['bot_config'] = $this->getBotConfigModel();

        $result['user'] = $this->command->userStatus($this->request->getUser());

        $result['message'] = $message;

        if (!empty($result)) return $result;

        return array('data'=>$message);
    }

    private function routeCallbackModel ($input) {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);

        return $input;
    }

    private function routeTextCommandModel ($input) {

        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $input['command']['text'] = (empty($input['command']['text']))?'/start':$input['command']['text'];

        $command = ($input['is_direct'])?$input['command']['text'].'-private':$input['command']['text'].'-opencommand';

        $this->logger->info('Command----------->>>'.$command);

        if (!empty($this->routes[$command]['function'])){

            $this->command = new Commands($this->logger,$this->request);

            $resultFunc = $this->command->callCommandFunction($this->routes[$command]['function'],$input);

        } else {
            return array('result'=>'error','text'=>"Can't find function for *".$input['command']."* command\r\n",'command'=>$input['command']);
        }

        if (empty($resultFunc['error'])) {
            return array('command'=>$input['command'],'data'=>$resultFunc,'route'=>$this->routes[$command]);
        } else {
            return array('result'=>'error','command'=>$input['command'],'text'=>$resultFunc['text']);
        }
    }
}