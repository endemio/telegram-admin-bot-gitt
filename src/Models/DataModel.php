<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 01.05.2018
 * Time: 3:03
 */

namespace SpamDeletingBot\Models;

use SpamDeletingBot\Controllers\Logger;
use Spyc;

class DataModel
{
    private $logger;

    public function __construct($logger) {

        $this->logger = (empty($logger))?new Logger():$logger;

   }

    public function getConfigModel($filename){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        if (file_exists($filename)) {
            return Spyc::YAMLLoad($filename);
        } else {
            $this->logger->error('No file exists->'.$filename);
            die ('No file exists->'.$filename);
        }
    }

}