<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 21.03.2018
 * Time: 18:48
 */

namespace SpamDeletingBot\Models;

use SpamDeletingBot\Controllers\Logger;

class TelegramModel
{
    protected $token;

    private $logger;

    public function __construct($config) {

        $this->logger = (empty($logger))?new Logger():$logger;

        $this->token = (!empty($config['token']))?$config['token']:'';
    }

    public function sendMessageModel($input) {
        if (!empty($input)) {
            $data = http_build_query($input);
            file_get_contents(TELEGRAM_URL.$this->token.'/sendMessage?'.$data.'&parse_mode='.PARSE_MODE.'&disable_web_page_preview=false');
        }
    }

    public function getChatMemberModel ($input) {

        if (!empty($input)) {
            $data = http_build_query($input);
            file_get_contents(TELEGRAM_URL.$this->token.'/getChatMember?'.$data);
        }
    }

    public function deleteMessageModel ($input) {
        if (!empty($input)) {
            $data = http_build_query($input);
            file_get_contents(TELEGRAM_URL.$this->token.'/deleteMessage?'.$data);
        }
    }

    public function kickOffMemberModel($input) {
        if (!empty($input)) {
            $data = http_build_query($input);
            file_get_contents(TELEGRAM_URL.$this->token.'/kickChatMember?'.$data);
        }
    }
}