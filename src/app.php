<?php
/**
 * Created by PhpStorm.
 * User: Дима
 */

$logger = new \SpamDeletingBot\Controllers\Logger();

# Starting Routes
    $route = new SpamDeletingBot\Controllers\Route($logger);
    $route->start();

# Start main Route
    $result   = $route->checkRoute();

# Starting View
    $view     = new SpamDeletingBot\Controllers\View();

# Turn on Telegram bot
    $telegram = new SpamDeletingBot\Controllers\Telegram($route->getBotConfig());

# Turn on keyboard
    $keyboard = new SpamDeletingBot\Controllers\Keyboard($logger);

# Store result
    if (DEBUG) $logger->info(print_R($result,true));

# If send command to Direct Message (allowed only for users from admins list)
    if ($result['user']['is_admin'] && $result['message']['is_direct']){

        $out = array(
            'chat_id'   => $result['message']['user']->id,
            'text'      => $view->view($result['route']['function'],$result),
            'reply_markup'  => $keyboard->setDefault()
        );

        $telegram->sendMessage($out);
        exit;
    }

# If send command to Direct Message (allowed only for users from admins list)
    if ($result['message']['is_command'] && !$result['message']['is_direct']){

        $out = array(
            'chat_id'   => $result['message']['chat_id'],
            'message_id' =>$result['message']['message_id']
        );
        $telegram->deleteMessage($out);

        $logger->info(PHP_EOL.'Message id='.$result['message']['message_id'].' by founded start command in message'.PHP_EOL);

        exit;
    }

# If get "spam" or "ban" message in reply from whitelist/admin user
    if ($result['user']['is_banned']){

        $logger->info(PHP_EOL.'Delete message from banned user. Message id= '.$result['message']['message_id'].PHP_EOL);
        $out = array(
            'chat_id'   => $result['message']['chat_id'],
            'message_id' =>$result['message']['message_id']
        );
        $telegram->deleteMessage($out);
        exit;
    }

# If setting /deletemessages to "on"
    if ($result['bot_config']['deletemessage'] == 'on' && !($result['user']['is_whitelist'] || $result['user']['is_admin'])){

        $remove = false;

        if ($result['message']['bad_words']     && $result['bot_config']['removebadwords']  == 'on') $remove = true;
        if ($result['message']['urls_message']  && $result['bot_config']['removeurls']      == 'on') $remove = true;
        if ($result['message']['urls_username'] && $result['bot_config']['removeurls']      == 'on') $remove = true;
        if ($result['message']['type']=='photo' && $result['bot_config']['removephoto']     == 'on') $remove = true;

        if ($remove){
            $out = array(
                'chat_id'   => $result['message']['chat_id'],
                'message_id' =>$result['message']['message_id']
            );

            $telegram->deleteMessage($out);
            $logger->info(PHP_EOL.'Delete message from by settings. Message id= '.$result['message']['message_id'].PHP_EOL);
            exit;
        }
    }

# If get "spam","spamall" or "ban" message in reply from whitelist/admin user
    if ($result['message']['type'] == 'reply_to_message' && ($result['user']['is_whitelist'] || $result['user']['is_admin'])){

        # Remove just one message
        if (strtolower($result['message']['text']) == 'spam'){
            $logger->info('Replied message as spam '.$result['message']['message_id']);
            $out = array(
                'chat_id'   =>  $result['message']['chat_id'],
                'message_id' => $result['message']['message_id']
            );
            $telegram->deleteMessage($out);

            $out = array(
                'chat_id'   =>  $result['message']['chat_id'],
                'message_id' => $result['message']['replied_message']['message_id']
            );
            $telegram->deleteMessage($out);
        }

        # Remove all message by this member
        if (strtolower($result['message']['text']) == 'spamall'){
            $logger->info('Replied message as spam '.$result['message']['message_id']);
            $out = array(
                'chat_id'   =>  $result['message']['chat_id'],
                'message_id' => $result['message']['message_id']
            );
            $telegram->deleteMessage($out);

            $posts = array_unique($route->listMessagesByUserId($result['message']['replied_message']['from']->id));

            krsort($posts);

            foreach ($posts as $key=>$value){
                $out = array(
                    'chat_id'   =>  $result['message']['chat_id'],
                    'message_id' => $value
                );
                $telegram->deleteMessage($out);

                unset ($posts[$key]);

                $route->storeListMessagesByUserId($result['message']['replied_message']['from']->id,$posts);
            }

            $logger->info(PHP_EOL.'Delete message from by SPAMALL. '.PHP_EOL);
        }

        # Ban user - he will not able to write
        if (strtolower($result['message']['text']) == 'ban'){
            if (!empty($result['message']['replied_message']['from']->id)) {
                $logger->info('Replied message sender->' . $result['message']['replied_message']['from']->id);
                $route->banUser($result['message']['replied_message']['from']->id);
            }
        }
        exit;
    }


# If new member join group
    if ($result['message']['type'] == 'new_chat_member'){

        $username = (!empty($result['message']['user']->first_name))?$result['message']['user']->first_name.' ':'';
        $username .= (!empty($result['message']['user']->last_name))?$result['message']['user']->last_name:'';
        $text = str_replace('{username}',$username,$result['bot_config']['welcome']);

        $out = array(
            'chat_id'   => $result['message']['chat_id'],
            'text'      => $text
        );
        $telegram->sendMessage($out);
        $logger->info(PHP_EOL.'Greeting new member '.PHP_EOL);
    }