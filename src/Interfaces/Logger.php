<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 14.08.2018
 * Time: 19:54
 */

namespace SpamDeletingBot\Interfaces;


interface Logger
{
    public function info($message);
    public function warn($message);
    public function error($message);
    public function store($input);

}