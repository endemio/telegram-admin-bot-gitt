<?php
/**
 * Class for making Keyboard for messages
 * Created by PhpStorm.
 * User: Дима
 * Date: 20.02.2018
 * Time: 0:59
 */

namespace SpamDeletingBot\Controllers;
use SpamDeletingBot\Models\KeyboardModel;

class Keyboard
{
    private $model; private $logger;

    public function __construct($logger) {
        # Logger
        $this->logger = (empty($logger))?new Logger():$logger;

        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $this->model = new KeyboardModel($logger);
    }


    public function setDefault () {
        return $this->model->setDefaultModel();
    }

}