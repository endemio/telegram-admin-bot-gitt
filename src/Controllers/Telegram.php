<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 14.03.2018
 * Time: 1:20
 */

namespace SpamDeletingBot\Controllers;
use SpamDeletingBot\Models\TelegramModel;

class Telegram
{
    private $model;

    private $logger;

    public function __construct($config) {

        # Logger
        $this->logger = (empty($logger))?new Logger():$logger;

        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        # Start Telegram Model
        $this->model = new TelegramModel($config);
    }

    public function sendMessage ($input){
        $this->model->sendMessageModel($input);
    }

    public function deleteMessage ($input){
        $this->model->deleteMessageModel($input);
    }

    public function kickOffMember ($input){
        $this->model->kickOffMemberModel($input);
    }

    public function getChatMember ($input){
        $this->model->getChatMemberModel($input);
    }
}