<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 17.05.2018
 * Time: 20:14
 */

namespace SpamDeletingBot\Controllers;


use SpamDeletingBot\Models\CommandsModel;

class Commands
{
    private $model; private $logger;

    public function __construct($logger,$request) {
        # Logger
        $this->logger = (empty($logger))?new Logger():$logger;

        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $this->model = new CommandsModel($logger,$request);
    }

    public function callCommandFunction($function,$data){
        return $this->model->callCommandFunctionModel($function,$data);
    }

    public function userStatus($user){
        return $this->model->userStatusModel($user);
    }

    public function getBotConfig(){
        return $this->model->getBotConfigModel();
    }

    public function banUser($id){
        return $this->model->banUserModel($id);
    }
}