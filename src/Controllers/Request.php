<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 22.03.2018
 * Time: 4:25
 */

namespace SpamDeletingBot\Controllers;


use SpamDeletingBot\Models\RequestModel;

class Request
{
    private $model; private $logger;

    public function __construct($logger) {
        # Logger
        $this->logger = (empty($logger))?new Logger():$logger;

        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $this->model = new RequestModel($logger);
    }

    public function storeRequest () {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->storeRequestModel();
    }

    public function getRequest () {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getRequestModel();
    }

    public function getCommand(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getCommandModel();
    }

    public function getCallback(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getCallbackModel();
    }

    public function getChatId() {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getChatIdModel();
    }

    public function getChatName() {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getChatNameModel();
    }

    public function getChatUserName() {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getChatUserNameModel();
    }


    public function getUser(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getUserDataModel();
    }

    public function getNewUsers(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getNewUsersModel();
    }

    public function getMessage(){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getMessageModel();
    }

    public function getMessageText (){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getMessageTextModel();
    }

    public function getMessageId () {
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getMessageIdModel();
    }

    public function getMessageType($input){
        if (DEBUG) $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->getMessageTypeModel($input);
    }

}