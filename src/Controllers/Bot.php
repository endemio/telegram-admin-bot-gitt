<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 23.04.2018
 * Time: 23:56
 */

namespace SpamDeletingBot\Controllers;

use SpamDeletingBot\Models\BotModel;

class Bot
{

    private $model;

    private $logger;

    public function __construct($logger){
        # Logger
        $this->logger = (empty($logger))?new Logger():$logger;

        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $this->model = new BotModel($logger);
    }


    public function getBotConfig(){
        return $this->model->getBotConfigModel();
    }

    public function getUserStatus($user){
        return $this->model->getUserStatusModel($user);
    }

    public function saveConfig($input){
        return $this->model->saveConfigModel($input);
    }

    public function changeBoolValue($key){
        return $this->model->changeBoolValueModel($key);
    }

}