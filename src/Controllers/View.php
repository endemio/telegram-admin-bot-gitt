<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 20.02.2018
 * Time: 15:19
 */

namespace SpamDeletingBot\Controllers;
use SpamDeletingBot\Models\ViewModel;


class View
{
    private $model; private $logger;

    public function __construct() {
        # Logger
        $this->logger = (empty($logger))?new Logger():$logger;

        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $this->model = new ViewModel();
    }

    public function view ($function,$input){
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        return $this->model->viewRouteModel($function,$input);
    }

}