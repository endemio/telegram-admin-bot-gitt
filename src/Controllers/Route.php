<?php
/**
 * Routes for returning values
 * Created by PhpStorm.
 * User: Дима
 */

namespace SpamDeletingBot\Controllers;

use SpamDeletingBot\Models\RouteModel;

class Route
{
    private $model; private $logger;

    public function __construct($logger) {
        # Logger
        $this->logger = (empty($logger))?new Logger():$logger;

        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $this->model = new RouteModel($logger);
    }

    public function start(){
        $this->model->startModel();
    }

    public function checkRoute(){
        return $this->model->checkRouteModel();
    }

    public function getBotConfig (){
        return $this->model->getBotConfigModel();
    }

    public function banUser($id){
        return $this->model->banUserModel($id);
    }

    public function listMessagesByUserId($id){
        return $this->model->listMessagesByUserIdModel($id);
    }

    public function storeListMessagesByUserId($id,$list){
        $this->model->storeListMessagesByUserIdModel($id,$list);
    }

}