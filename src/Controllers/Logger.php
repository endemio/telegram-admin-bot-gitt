<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 15.07.2018
 * Time: 2:25
 */

namespace SpamDeletingBot\Controllers;

use SpamDeletingBot\Models\LoggerModel;

class Logger implements \SpamDeletingBot\Interfaces\Logger
{
    private $model;

    public function __construct()  {
        $this->model = new LoggerModel();
    }

    public function info($message){
        $this->model->infoModel($message);
    }

    public function error($message){
        $this->model->errorModel($message);
    }

    public function warn($message){
        $this->model->warnModel($message);
    }

    public function store($input){
        $this->model->storeModel($input);
    }

}