<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 01.05.2018
 * Time: 3:03
 */

namespace SpamDeletingBot\Controllers;

use SpamDeletingBot\Models\DataModel;

class Data
{
    private $model; private $logger;

    public function __construct($logger)
    {
        # Logger
        $this->logger = (empty($logger))?new Logger():$logger;

        $this->logger->info(__CLASS__.' '.__FUNCTION__);

        $this->model = new DataModel($logger);
    }

    public function getYamlFile($filename){
        return $this->model->getConfigModel($filename);
    }
}