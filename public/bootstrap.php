<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 19.05.2018
 * Time: 15:59
 */

# Turn on showing error
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('UTC');

define('DEBUG', true);

define('TELEGRAM_URL','https://api.telegram.org/bot');

define('TELEGRAM_NEW_LINE',"\n");

define('PARSE_MODE',"HTML");

// Main Dir
define('ROOT', __DIR__.DIRECTORY_SEPARATOR);

// Set data dir
define('DATA_DIR', ROOT.'../data'.DIRECTORY_SEPARATOR);

// Set data dir
define('LOG_DIR', ROOT.'../data/log'.DIRECTORY_SEPARATOR);

// Set config dir
define('CONFIG_DIR', ROOT.'../config'.DIRECTORY_SEPARATOR);

// Set config dir
define('CONFIG_ARCHIVE_DIR', CONFIG_DIR.'archive'.DIRECTORY_SEPARATOR);

// Set vendor dir
define('VENDOR_DIR', ROOT.'../vendor');

// Set src dir
define('SRC_DIR', ROOT.'../src'.DIRECTORY_SEPARATOR);

// Set src dir
define('ROUTES_CONFIG', 'routes.yaml');