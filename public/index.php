<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 20.08.2018
 * Time: 14:07
 */

require ('bootstrap.php');

// Autoload.php
if (file_exists(VENDOR_DIR.'/autoload.php')) {
    require VENDOR_DIR . '/autoload.php';
} else {
    die ('No autoload file');
}

// All app setup here
if (file_exists(SRC_DIR.'/app.php')) {
    require SRC_DIR . '/app.php';
} else {
    die ('No application file');
}