### Installing bot ###

* Clone this repo to server
* Make *composer update*
* In  */config/config-main.yaml* change to new value:
    * bot token (token:)
    * add username to admins. Lately it can be changed in bot 
* Set Webhooks by https://api.telegram.org/botTOKEN/setwebhook?url=https_url_to_public_folder
 
### Instruction ###
* If you admin (you username in config-main.yaml as admins) - add bot to contact, press "start" and you will see Bot Commands

```
    /start - Start bot
    /help - Show bot function
    /settings - Show Boolean Settings
    /showwelcome - Show welcome message
    /setwelcome - Show welcome message
    /showwl - Show whitelist
    /showadmin - Show Admins list
    /showbanned - Show banned users
    /addwl - Add user to whitelist
    /addadmin - Add user to Admins
    /removewl - Remove user from Whitelist
    /removeadmin - Remove user from Admins
    /unban - Remove user from Banned
    /kickoff - Removing users from Group
    /deletemessage - Removing messages from Group
    /removephoto - Removing messages with Photo
    /removeurl - Removing messages with URLs
    /removebad - Removing messages with Bad words
```

* Choose command and you will se what it does

* Only admin user (from /showadmin) can make changes to bot settings. Users from /whitelist can:
     - ban user by reply to message and write "ban" (without ")
     - remove spam messages from group by reply to message and write "spam" (without ") 
     - remove all users messages from group by reply to message and write "spamall" (without "). Bot can remove only messages, which was sent to group AFTER time you add bot in group
     
### Debug ###
* Turn on debug mode in */public/bootstrap.php*
```php
define('DEBUG', true);
```     
* Open *https_url_to_public_folder* and bot will get test requests from */data/test-request/request.yaml*. If there is more then one requests - it get randomly 
* All old config will be stored in */config/archive*